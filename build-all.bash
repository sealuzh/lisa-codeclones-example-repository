#!/bin/bash

echo "--- Compiling and running C# code ---"
cd csharp
xbuild csharp4example.csproj
cd ..

echo "--- Compiling and running Java code ---"
cd java
ant all
cd ..

echo "--- Running Javascript code ---"
cd javascript
node src/main.js
cd ..


