using System;
using System.Diagnostics;

namespace org.example {

    public class Hello
    {
        public static void Main(string[] args)
        {

            ConcreteClass instance = new ConcreteClass();
            ConcreteClass instanceConstructedWithParams = new ConcreteClass(1);
            ConcreteClass.InnerClass instanceInnerClass = new ConcreteClass.InnerClass();
            ConcreteClass.InnerClass.InnerInnerClass instanceInnerInnerClass = new ConcreteClass.InnerClass.InnerInnerClass();
            ConcreteClass nullVar = null;

            instance.invokesMethodMethod();

            double one = Math.Cos(Math.PI * 2);
            float two = 2f;

            int x = 10;
            while (x > 0) {
                x--;
                --x;
            }
            Debug.Assert(x <= 0);

            bool assertionThing = false;
            if (1 > 2) {
            }
            else {
                if (200 > 100) {
                    if (2000 > 1000) {
                        assertionThing = true;
                    }
                }
            }
            Debug.Assert(assertionThing == true);

            Debug.Assert(instance.returnsSomethingMethod() == 1);

            System.Console.WriteLine("Main function finished");

        }
    }

}
