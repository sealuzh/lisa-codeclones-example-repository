package ch.uzh.ifi.seal.example;

import static java.lang.Math.*;

public class Main {
  public static void main(String[] args) {

    ConcreteClass instance = new ConcreteClass();
    ConcreteClass instanceConstructedWithParams = new ConcreteClass(1);
    ConcreteClass.InnerClass instanceInnerClass = instance.new InnerClass();
    ConcreteClass.InnerClass.InnerInnerClass instanceInnerInnerClass = instanceInnerClass.new InnerInnerClass();
    ConcreteClass nullVar = null;

    instance.invokesMethodMethod();

    double one = cos(PI * 2);
    float two = 2f;

    int x = 10;
    while (x > 0) {
      x--;
      --x;
    }
    assert(x <= 0);

    boolean assertionThing = false;
    if (1 > 2) {
    }
    else {
      if (200 > 100) {
        if (2000 > 1000) {
          assertionThing = true;
        }
      }
    }
    assert(assertionThing == true);

    assert(instance.returnsSomethingMethod() == 1);

    System.out.println("Main function finished");

  }
}

