module.exports = {
  Concrete: Concrete
}

function Concrete() {
  var instanceVariable;

  var privateField = 1;
  var protectedField = 1;
  var publicField = 1;

  var privateStaticField = 1;
  var protectedStaticField = 1;
  var publicStaticField = 1;

  function privateMethod() { }

  function protectedMethod() { }

  this.mccMethod = function() {
    if (true) {
      if (true) {
        if (true) {
        }
      }
    }
  }

  this.accessesFieldsMethod = function() {
    privateField++;
    protectedField++;
    publicField++;
  }

  this.hasParametersMethod = function(s, c, i, d, f, l, sh, b) { }

  this.returnsSomethingMethod = function() { return 1; }

  this.invokesMethodMethod = function() {
    this.returnsSomethingMethod();
  }

  this.Concrete = function() {
    instanceVariable = 1;
  }

  this.Concrete = function(constructorParam) {
    instanceVariable = constructorParam;
  }

  this.InnerClass = function () {

    function f1() { return 1; }

    this.InnerInnerClass = function () {
      function f2() { return 2; }
      function f3() { if (true) { return 3; } return 4; }
    }

  }

  this.addedMethod1 = function() { }

}

