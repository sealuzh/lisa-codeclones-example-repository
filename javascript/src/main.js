var concrete = require('./concrete.js');

function Main() {

  var instance = new concrete.Concrete();
  var instanceConstructedWithParams = new concrete.Concrete(1);
  var instanceInnerClass = new instance.InnerClass();
  var instanceInnerInnerClass = new instanceInnerClass.InnerInnerClass();
  var nullVar = null;

  instance.invokesMethodMethod();

  var one = Math.cos(Math.PI * 2);

  var x = 10;
  while (x > 0) {
    x--;
    --x;
  }

  var assertionThing = false;
  if (1 > 2) {
  }
  else {
    if (200 > 100) {
      if (2000 > 1000) {
        assertionThing = true;
      }
    }
  }

  console.log("Main function finished");
}

Main()
